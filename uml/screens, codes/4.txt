@startuml
class UserInfo{
FirstName
SeconName
ect. info
}

class DataInfo{
Login
Password
}
class StatusInfo{
Status
}

class OtherInfo{
Adress
PhoneNumber
}

UserInfo--> DataInfo
UserInfo--> StatusInfo
UserInfo--> OtherInfo
@enduml