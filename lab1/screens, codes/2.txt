@startuml
left to right direction

actor  "Сервер" as SE
circle "Обробка запиту" as OB

usecase AV[
<b>Авторизація в системі]

usecase RE[
<b>Реєстрація аккаунту]

usecase SN[
<b>Надсилання сповіщення]

usecase SQ[
<b>Отримання повідомлення]

usecase SL[
<b>Пошук сервісу]

usecase SS[
<b>Відображення статусу користувача

]

usecase CI[
<b>Відображення інфи про користувача]



AV <-- SE
RE <-- SE
SL <-- SE
SN <-- SE
SQ <-- SE
CI <-- SE
SS <-- SE

SE --> OB
usecase BLP[
<b>БД (login/password)]

usecase BIP[
<b>БД (Інфа про  юзера)]

usecase BSM[
<b>БД (Черга юзера\інфа статусу юзера]
OB --> BLP
OB --> BIP
OB --> BSM
@enduml