import sys
from PyQt5 import QtWidgets
from win_login import Ui_LoginWindow
from win_register import Ui_RegisterWindow
from win_adminlogin import Ui_AdminLoginWindow
from mainuser import *
from mainadmin import MainAdminWindow
from data_base import registration, user_loging, admin_loging, request_id, request_admin_id



class AdminWindow(QtWidgets.QMainWindow, Ui_AdminLoginWindow):
    def __init__(self, a):
        super().__init__()
        self.setupUi(self)

        self.a = a



        self.action_main.triggered.connect(self.goto_main)
        self.button_login.clicked.connect(self.goto_loging)

    def goto_loging(self):
        if self.line_password.text() and self.line_login.text():
            request = admin_loging(self.line_login.text(), self.line_password.text())
            if request:
                QtWidgets.QMessageBox.information(
                    self, "Completed", "You have successfully logged in to your account")
                request_go = request_admin_id(self.line_login.text())
                self.goto_login(request_go)
            else:
                QtWidgets.QMessageBox.information(
                    self, "Error", "Password or login is incorrect.")

        else:
            QtWidgets.QMessageBox.information(
                self, 'Information', 'Please fill in all fields correctly.')

    def goto_login(self, request_go):
        self.mainadminwindow = MainAdminWindow(request_go)
        self.mainadminwindow.show()
        self.hide()

    def goto_main(self):
        if self.a == 1:
            self.loginwindow = LoginWindow()
            self.loginwindow.show()
            self.hide()
        else:
            self.registerwindow = RegisterWindow()
            self.registerwindow.show()
            self.hide()


class RegisterWindow(QtWidgets.QMainWindow, Ui_RegisterWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.action_admin.triggered.connect(self.goto_admin)

        self.button_continue.clicked.connect(self.goto_register)
        self.button_login.clicked.connect(self.goto_login)

    def goto_register(self):
        self.check_data()

    def check_data(self):
        if self.line_fname.text() and \
                self.line_sname.text() and \
                self.line_email.text() and \
                self.line_phone.text() and \
                self.line_password.text() and \
                self.line_2password.text():
            if self.line_password.text() == self.line_2password.text():
                request = registration(self.line_fname.text(), self.line_sname.text(),
                                       self.line_email.text(), self.line_phone.text(),
                                       self.line_password.text())
                if request is True:
                    QtWidgets.QMessageBox.information(
                        self, "Completed", "You have successfully registered.")
                    self.error.setStyleSheet("color: rgb(0, 170, 0);;")
                    self.error.setText('You have successfully registered an account.')

                    self.goto_login()
                else:
                    self.error.setStyleSheet("color: rgb(214, 0, 3);")
                    self.error.setText('An account with this EMAIL already exists!')
                    QtWidgets.QMessageBox.warning(
                        self, "Information", "Please fill in all fields correctly.")
            else:
                QtWidgets.QMessageBox.information(
                    self, "Password and Confirm password don\'t match")
        else:
            self.error.setText('Please fill in all fields')

    def goto_login(self):
        self.loginwindow = LoginWindow()
        self.loginwindow.show()
        self.hide()

    def goto_admin(self):
        self.adminwindow = AdminWindow(2)
        self.adminwindow.show()
        self.hide()


class LoginWindow(QtWidgets.QMainWindow, Ui_LoginWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)


        self.registerwindow = RegisterWindow()
        self.adminwindow = AdminWindow(1)

        self.action_admin.triggered.connect(self.goto_admin)
        self.button_login.clicked.connect(self.goto_loging)
        self.button_reg.clicked.connect(self.goto_registration)

    def goto_loging(self):
        if self.line_login.text() and self.line_password.text():
            if user_loging(self.line_login.text(), self.line_password.text()):
                QtWidgets.QMessageBox.information(
                    self, "Completed", "You have successfully logged in to your account")
                request = request_id(self.line_login.text())

                self.goto_login(request)
            elif user_loging(self.line_login.text(), self.line_password.text()) is False:
                QtWidgets.QMessageBox.information(
                    self, "Error", "Password or login is incorrect.")
            else:
                QtWidgets.QMessageBox.warning(
                    self, "Information", "Such an account does not exist.")
        else:
            QtWidgets.QMessageBox.information(
                self, "Information", "Please fill in all fields correctly.")

    def goto_login(self, request):
        self.mainuserwindow = MainUserWindow(request)
        self.mainuserwindow.show()
        self.hide()

    def goto_admin(self):
        self.adminwindow.show()
        self.hide()

    def goto_registration(self):
        self.registerwindow.show()
        self.hide()

    def closeEvent(self, event):
        exit_msg = 'Are you sure you want to exit?'
        resp = QtWidgets.QMessageBox.question(self, 'Confirm exit', exit_msg,
                                              QtWidgets.QMessageBox.Yes,
                                              QtWidgets.QMessageBox.No)
        if resp == QtWidgets.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


def run_app():
    app = QtWidgets.QApplication(sys.argv)
    wn = LoginWindow()
    wn.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    run_app()
