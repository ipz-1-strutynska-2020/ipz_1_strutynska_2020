import sys
from PyQt5 import QtWidgets
from PyQt5.QtCore import QSize
from PyQt5.QtWidgets import QTableWidgetItem, QLabel
from PyQt5.QtGui import QIcon
# import windows:
from win_mainadmin import Ui_MainAdminWindow
from data_base import get_user_reg_info_admin



class MainAdminWindow(QtWidgets.QMainWindow, Ui_MainAdminWindow):
    def __init__(self, request):
        super().__init__()
        self.setupUi(self)
        self.actionexit.triggered.connect(self.goto_exit)
        self.get_info()


        # item = QTableWidgetItem()
        # self.tableWidget.setItem(0, 0, item)
        # item.setIcon(QIcon("111.jpg"))
        # self.tableWidget.setIconSize(QSize(100, 100))


    def get_info(self):
        get_info = get_user_reg_info_admin()
        if get_info:
            row_num = 0
            for row in get_info:
                col = 0
                for item in row:
                    cellinfo = QTableWidgetItem(str(item))
                    self.tableWidget.setItem(row_num, col, cellinfo)
                    col += 1
                row_num += 1
        else:
            self.tableWidget.hide()





    def goto_exit(self):
        print('exit')

def run_app():
    app = QtWidgets.QApplication(sys.argv)
    wn = MainAdminWindow()
    wn.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    run_app()
