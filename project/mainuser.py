from PyQt5 import QtWidgets
from data_base import get_user_reg_info,register_user, go_reg_queue
from win_mainuser import Ui_MainUserWindow
from PyQt5.QtWidgets import QTableWidgetItem
from main import *
import time

class MainUserWindow(QtWidgets.QMainWindow, Ui_MainUserWindow):
    def __init__(self, request):
        super().__init__()
        self.setupUi(self)

        self.data_processing(request[0])

        self.action_main.triggered.connect(self.goto_exit)

        request_id = request[0]
        request_name = request[1]
        request_2name = request[2]
        request_email = request[3]
        request_phone = request[4]
        self.set_label_id.setText('ID: ' + str(request_id))
        self.set_label_name.setText(str(request_name))
        self.set_label_name_2.setText(str(request_2name))
        self.set_label_email.setText(str(request_email))
        self.set_label_phone.setText(str(request_phone))

        self.button_register.clicked.connect(self.register_queue)



    def move_time(self, time):
        new_time = time.replace(':', '')
        return new_time

    def register_queue(self):
        removed_time = self.move_time(str(self.combo_time.currentText()))
        request = register_user(str(self.combo_day.currentText()), removed_time)
        if not request:
            go_reg_queue(self.set_label_id.text()[3:], removed_time, str(self.combo_day.currentText()))
            QtWidgets.QMessageBox.information(
                self, "Completed", "You have successfully registered in the queue")
        else:
            QtWidgets.QMessageBox.information(
                self, "Error", "The queue is busy. Choose a different date or time")


    def data_processing(self, request_id):
        get_info = get_user_reg_info(request_id)
        if get_info:
            self.tableWidget.show()
            self.label_getinfo_error.setText('')
            # [(25, 1, '1600', '05.01', '2021-01-03 11:59:08'), (25, 3, '1130', '06.01', '2021-01-03 10:25:08')]
            row_num = 0
            for row in get_info:
                col = 0
                for item in row:
                    cellinfo = QTableWidgetItem(str(item))
                    self.tableWidget.setItem(row_num, col, cellinfo)
                    col += 1
                row_num += 1
        else:
            self.tableWidget.hide()
            self.label_getinfo_error.setText('Ви не зареєстровані в жодній онлайн черзі.')




    def goto_exit(self):
        self.loginwindow = LoginWindow()
        self.loginwindow.show()
        self.hide()

def run_app():

    app = QtWidgets.QApplication(sys.argv)
    wn = MainUserWindow()
    wn.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    run_app()
